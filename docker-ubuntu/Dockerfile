FROM ubuntu:latest

MAINTAINER samplecigroup <samplecigroup@gmail.com>

# This is required because Ubuntu installation is interactive
# even if the -y is provided (sic)
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y \
        build-essential \
        clang \
        clang-tools \
        cmake \
        doxygen \
        gcc \
        git \
        lcov \
        libcmocka-dev \
        libnss-wrapper \
        libpam-dev \
        libpam-wrapper \
        libresolv-wrapper \
        libsocket-wrapper \
        libuid-wrapper \
        llvm \
        make \
        python3-dev \
        valgrind \
        curl \
        clang-tidy \
        clang-format\
        gcc-multilib \
        python3-venv \
        automake bison flex g++ git libboost-all-dev libevent-dev libssl-dev \
        nodejs npm libtool make pkg-config python3-pip cppcheck xz-utils tar
RUN pip3 install virtualenv
RUN cd /opt \
    && git clone https://github.com/Ericsson/CodeChecker.git --depth 1 \
    && cd CodeChecker \
    && make package 
COPY . /CodeChecker
ENV PATH="/opt/CodeChecker/build/CodeChecker/bin:$PATH"
RUN echo $PATH
RUN CodeChecker --help
RUN VERSION=1.1.0; \
    curl -sSL "https://github.com/facebook/infer/releases/download/v$VERSION/infer-linux64-v$VERSION.tar.xz" \
    | tar -C /opt -xJ && \
    ln -s "/opt/infer-linux64-v$VERSION/bin/infer" /usr/local/bin/infer
RUN infer --version
RUN rm -rf /var/lib/apt/lists/*
